# status



## Instalação

Tenha o [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) instalado na sua máquina.

> Use o node 12.22.9, instalado via [nvm](https://github.com/nvm-sh/nvm).

Então faça o clone e instale.

```
git clone https://gitlab.com/rg3915/status.git
cd status
npm install
npm run serve
```

